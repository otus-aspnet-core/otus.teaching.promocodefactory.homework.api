﻿using System;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class GrpcCustomersController
        : ControllerBase
    {
        private readonly CustomersService.CustomersServiceClient _customersServiceClient;

        public GrpcCustomersController(CustomersService.CustomersServiceClient customersServiceClient)
        {
            _customersServiceClient = customersServiceClient;
        }

        [HttpGet]
        public async Task<GrpcCustomerShortResponseList> GetCustomersAsync()
        {
            return await _customersServiceClient.GetCustomersAsync(new Empty());
        }

        [HttpGet("{id:guid}")]
        public async Task<GrpcCustomerResponse> GetCustomerAsync(Guid id)
        {
            return await _customersServiceClient.GetCustomerAsync(new GrpcGuidId {Id = id.ToString()});
        }

        [HttpPost]
        public async Task<GrpcCustomerResponse> CreateCustomerAsync(GrpcCreateOrEditCustomerRequest request)
        {
            return await _customersServiceClient.CreateCustomerAsync(request);
        }

        [HttpPut("{id:guid}")]

        public async Task<IActionResult> EditCustomerAsync(Guid id, GrpcCreateOrEditCustomerRequest request)
        {
            try
            {
                await _customersServiceClient
                    .EditCustomerAsync(new GrpcEditCustomerRequest
                    {
                        Id = new GrpcGuidId
                        {
                            Id = id.ToString()
                        }, 
                        Request = request
                    });
            }
            catch (RpcException e) when (e.StatusCode == Grpc.Core.StatusCode.NotFound) 
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            try
            {
                await _customersServiceClient.DeleteCustomerAsync(new GrpcGuidId {Id = id.ToString()});
            }
            catch (RpcException e) when (e.StatusCode == Grpc.Core.StatusCode.NotFound) 
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Protos
{
    public class GrpcCustomersService : CustomersService.CustomersServiceBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public GrpcCustomersService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GrpcCustomerShortResponseList> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var result = new GrpcCustomerShortResponseList();

            result.Customers.AddRange(customers.Select(x => new GrpcCustomerShortResponse()
            {
                Id = x.Id.ToString(),
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }));

            return result;
        }

        public override async Task<GrpcCustomerResponse> GetCustomer(GrpcGuidId request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var customerId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Некорректный Id клиента"));
            }

            var customer = await _customerRepository.GetByIdAsync(customerId);

            var response = new GrpcCustomerResponse
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };

            response.Preferences.AddRange(customer.Preferences.Select(x => new GrpcPreferenceResponse
            {
                Id = x.PreferenceId.ToString(),
                Name = x.Preference.Name
            }));

            return response;
        }

        public override async Task<GrpcCustomerResponse> CreateCustomer(GrpcCreateOrEditCustomerRequest request,
            ServerCallContext context)
        {
            var preferencesIds = request.PreferencesIds
                .Select(x =>
                {
                    if (!Guid.TryParse(x.Id, out var id))
                    {
                        throw new RpcException(new Status(StatusCode.InvalidArgument, "Некорректный Id предпочтения"));
                    }

                    return id;
                })
                .ToList();

            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferencesIds);

            var customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return await GetCustomer(new GrpcGuidId {Id = customer.Id.ToString()}, context);
        }

        public override async Task<Empty> EditCustomer(GrpcEditCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id.Id, out var customerId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Некорректный Id клиента"));
            }

            var customer = await _customerRepository.GetByIdAsync(customerId);
            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Не найден клиент"));
            }

            var preferencesIds = request.Request.PreferencesIds
                .Select(x =>
                {
                    if (!Guid.TryParse(x.Id, out var id))
                    {
                        throw new RpcException(new Status(StatusCode.InvalidArgument, "Некорректный Id предпочтения"));
                    }

                    return id;
                })
                .ToList();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(preferencesIds);

            customer = CustomerMapper.MapFromModel(request.Request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(GrpcGuidId request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var customerId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Некорректный Id клиента"));
            }

            var customer = await _customerRepository.GetByIdAsync(customerId);
            if (customer == null)
            {
                throw new RpcException(new Status(StatusCode.NotFound, "Не найден клиент"));
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}